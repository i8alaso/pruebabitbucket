package org.prueba1;

public class BinaryTreeNode {
	
	private int value;
	private BinaryTreeNode leftChild;
	private BinaryTreeNode rightNode;
	
			
	public BinaryTreeNode(int value, BinaryTreeNode leftChild,	BinaryTreeNode rightNode) {
		this.value = value;
		this.leftChild = leftChild;
		this.rightNode = rightNode;
	}

	public int getValue() {
		return value;
	}
		
	public void setValue(int value) {
		this.value = value;
	}
	
	public BinaryTreeNode getLeftChild() {
		return leftChild;
	}
	
	public void setLeftChild(BinaryTreeNode leftChild) {
		this.leftChild = leftChild;
	}
	
	public BinaryTreeNode getRightNode() {
		return rightNode;
	}
	
	public void setRightNode(BinaryTreeNode rightNode) {
		this.rightNode = rightNode;
	}
	
	
}
